package com.example.chessgame;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;

import java.util.ArrayList;

/**
 * @author Daniel Lago Riomao
 * @version 1.2
 */
public class DataBase extends SQLiteOpenHelper {

    public SQLiteDatabase sqlLiteDB;
    public final static String NOME_BD = "puzzles.db";
    public final static int VERSION_BD = 1;
    private String CREAR_TABOA_PUZZLE = "CREATE TABLE PUZZLE ( " +
            "idPuzzle  INTEGER PRIMARY KEY," +
            "inicioTableiroFen VARCHAR(200)," +
            "dificultade INTEGER," +
            "corXogador VARCHAR(10)," +
            "resolto BOOLEAN," +
            "solucionMoves VARCHAR(40))";

    public DataBase(Context context) {
        super(context, NOME_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREAR_TABOA_PUZZLE);
        } catch (Exception e) {
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS PUZZLE");
        onCreate(db);
    }

    public void addPuzzle(Puzzle puzzle) {
        ContentValues valores = new ContentValues();
        valores.put("idPuzzle", puzzle.getIdPuzzle());
        valores.put("inicioTableiroFen", puzzle.getFen());
        valores.put("dificultade", puzzle.getDificultad());
        valores.put("corXogador", puzzle.getCorXogador());
        valores.put("resolto", puzzle.isResolto());
        if (puzzle.getSolucion().size() == 1)
            valores.put("solucionMoves", puzzle.getSolucion().get(0).toString().toUpperCase());
        else {
            String value = "";
            for (Move move : puzzle.getSolucion()) {
                value += move.toString().toUpperCase() + "/";
            }
            valores.put("solucionMoves", value);
        }

        try {
            sqlLiteDB.insertOrThrow("PUZZLE", null, valores);
        } catch (android.database.sqlite.SQLiteConstraintException e) {
        }
    }

    public Puzzle getPuzzle(int idPuzzle) {
        String[] parametros = new String[]{String.valueOf(idPuzzle)};
        Cursor datosConsulta = sqlLiteDB.rawQuery("select * from PUZZLE where idPuzzle = ?", parametros);

        if (datosConsulta.moveToFirst()) {
            ArrayList<Move> solution = new ArrayList<Move>();
            Puzzle puzzle = null;
            puzzle = new Puzzle();
            puzzle.setIdPuzzle(datosConsulta.getInt(0));
            puzzle.setFen(datosConsulta.getString(1));
            puzzle.setDificultad(datosConsulta.getInt(2));
            puzzle.setCorXogador(datosConsulta.getString(3));
            puzzle.setResolto(datosConsulta.getInt(4) > 0);
            String movesString = datosConsulta.getString(5);
            if (movesString.contains("/")) {
                String[] split = movesString.split("/");
                for (String string : split) {
                    String square1S = string.substring(0, 2);
                    String square2S = string.substring(2);

                    solution.add(stringsToMove(square1S, square2S));
                }
            } else {
                String square1S = movesString.substring(0, 2);
                String square2S = movesString.substring(2);

                solution.add(stringsToMove(square1S, square2S));
            }
            puzzle.setSolucion(solution);

            return puzzle;
        }
        return null;
    }

    public ArrayList<Puzzle> getPuzzles() {
        ArrayList<Puzzle> puzzles = new ArrayList<Puzzle>();

        Cursor datosConsulta = sqlLiteDB.rawQuery("select * from PUZZLE order by dificultade", null);
        if (datosConsulta.moveToFirst()) {
            Puzzle puzzle = null;
            while (!datosConsulta.isAfterLast()) {
                ArrayList<Move> solution = new ArrayList<Move>();
                puzzle = new Puzzle();
                puzzle.setIdPuzzle(datosConsulta.getInt(0));
                puzzle.setFen(datosConsulta.getString(1));
                puzzle.setDificultad(datosConsulta.getInt(2));
                puzzle.setCorXogador(datosConsulta.getString(3));
                puzzle.setResolto(datosConsulta.getInt(4) > 0);
                String movesString = datosConsulta.getString(5);
                if (movesString.contains("/")) {
                    String[] split = movesString.split("/");
                    for (String string : split) {
                        String square1S = string.substring(0, 2);
                        String square2S = string.substring(2);

                        solution.add(stringsToMove(square1S, square2S));
                    }
                } else {
                    String square1S = movesString.substring(0, 2);
                    String square2S = movesString.substring(2);

                    solution.add(stringsToMove(square1S, square2S));
                }
                puzzle.setSolucion(solution);
                puzzles.add(puzzle);

                datosConsulta.moveToNext();
            }
        }

        return puzzles;
    }

    /**
     * Cambia o valor de "resolto" dentro da base de datos a true do puzzle indicado.
     * @param puzzle Puzzle ao cal se lle quere modificar a true o valor resolto.
     */
    public void setPuzzleSolved(Puzzle puzzle) {
        ContentValues cv = new ContentValues();
        cv.put("resolto",true);

        sqlLiteDB.update("PUZZLE", cv, "idPuzzle = ?", new String[]{String.valueOf(puzzle.getIdPuzzle())});

    }

    /**
     * Pasa dúas casillas en formato String para devolver un Move coas súas respectivas Square.
     * @param square1S Casilla desde onde queres que empeze a moverse.
     * @param square2S Casilla a cál queres que se faga o movemento.
     * @return Obxecto Move coas casillas indicadas.
     */
    private Move stringsToMove(String square1S, String square2S) {
        Square square1 = null;
        Square square2 = null;

        for (Square square : Square.values()) {
            if (square.toString().equals(square1S)) {
                square1 = square;
                continue;
            }
            if (square.toString().equals(square2S)) {
                square2 = square;
                continue;
            }
        }

        return new Move(square1, square2);
    }

}
