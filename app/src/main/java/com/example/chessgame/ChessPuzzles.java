package com.example.chessgame;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteConstraintException;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.VolumeShaper;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.util.Xml;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.distribute.Distribute;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import com.github.bhlangonijr.chesslib.Board;
import com.github.bhlangonijr.chesslib.BoardEvent;
import com.github.bhlangonijr.chesslib.BoardEventListener;
import com.github.bhlangonijr.chesslib.BoardEventType;
import com.github.bhlangonijr.chesslib.Piece;
import com.github.bhlangonijr.chesslib.PieceType;
import com.github.bhlangonijr.chesslib.Rank;
import com.github.bhlangonijr.chesslib.Side;
import com.github.bhlangonijr.chesslib.Square;
import com.github.bhlangonijr.chesslib.move.Move;
import com.microsoft.appcenter.distribute.Distribute;

import org.xmlpull.v1.XmlPullParser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author Daniel Lago Riomao
 * @version 2.0
**/
public class ChessPuzzles extends AppCompatActivity {

    public enum TIPOREDE {MOBIL, ETHERNET, WIFI, SENREDE}
    private boolean finalMove;
    private String XML = "https://a19daniellr.weebly.com/uploads/1/3/6/0/136098059/puzzles.xml";
    private Square square1;
    private Square square2;
    private ImageView from;
    private ImageView to;
    private Board board;
    private int cont;
    private TextView textViewPlayer;
    private Button buttonUndo, buttonChangePuzzle, buttonExit;
    private Animation scaleUp, scaleDown;
    private TextView textViewGame;
    private TextView textViewTitle;
    private TIPOREDE conexion;
    private Puzzle actual;
    private ArrayList<Square> squaresLegals;
    public static DataBase dataBase;
    private File rutaArquivo;
    private ArrayList<Move> solucionPuzzle;
    private ArrayList<Puzzle> puzzles;
    private MediaPlayer moveSound;
    private boolean capture;
    private MediaPlayer moveCheck;
    private MediaPlayer moveCapture;
    private MediaPlayer moveWIN;

    /**
     * É o listener do botón para cambiar a Actividade dos puzzles.
     * @param view Vista a que se lle aplica (Button neste caso).
     */
    public void onClickChangePuzzle(View view) {
        if (dataBase == null || dataBase.getPuzzles().isEmpty()) {
            Toast.makeText(getApplicationContext(), getString(R.string.toast_change_puzzle), Toast.LENGTH_LONG).show();
        }

        Intent intent = new Intent();
        intent.setClass(getApplicationContext(), PuzzlesView.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        startActivity(intent);
    }

    /**
     * Devolve A ImageView da vista do puzzle (do xml[activity_main.xml]) pasandolle un Square (casilla).
     * @param square Casilla deseada para coller o seu respectivo ImageView do XML
     * @return ImageView deseada do XML
     */
    private ImageView getImageViewSquareLocation(Square square) {
        GridLayout layout = (GridLayout) findViewById(R.id.grid_panel);

        for (int i = 0; i < layout.getChildCount(); i++) {
            View subView = layout.getChildAt(i);
            if (subView instanceof ImageView) {
                ImageView imageView = (ImageView) subView;
                if (getResources().getResourceEntryName(imageView.getId()).toString().equals(square.toString())) {
                    return imageView;
                }
            }
        }
        // NO HAI COINCIDENCIAS, MALO PORQUE TEN QUE HABELAS
        return null;
    }

    /** Añade os listeners as imageviews, o sea para cando tocas a peza,
     * saber que peza é, se é da túa cor, e se se pode mover e demáis.
     *
     * Recomendase non tocar este método a menos que se vaia a crear outro tipo de movemento de pezas (Arrastrando, por exemplo).
     * Isto débese a que xa están todas as funcións implementadas como: coronar a raiña, comer EN PASSANT, enrrocar...
     * Se se varía algo dentro podería complicar moito as cousas.
     */
    private void addListeners() {
        for (Square square : Square.values()) {
            if (square.equals(Square.NONE)) continue;
            getImageViewSquareLocation(square).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (square1 == null) {
                        squaresLegals = new ArrayList<Square>();
                        if (getImageViewSquareLocation(square).getDrawable() != null) {
                            square1 = square;
                            List<Move> moves = board.legalMoves();
                            for (Move move : moves) {
                                if (move.getFrom().equals(square1)) {
                                    squaresLegals.add(move.getTo());
                                }
                            }
                            for (Square square3 : squaresLegals) {
                                if (square3.isLightSquare()) {
                                    getImageViewSquareLocation(square3).setBackgroundColor(Color.LTGRAY);
                                } else {
                                    getImageViewSquareLocation(square3).setBackgroundColor(Color.GRAY);
                                }
                            }
                            if (squaresLegals.isEmpty()) {
                                square1 = null;
                            }
                        }
                    } else {

                        square2 = square;
                        if (!squaresLegals.contains(square)) {
                            if (getImageViewSquareLocation(square).getDrawable() != null) {
                                for (Square square3 : squaresLegals) {
                                    if (from != null || to != null) {
                                    if (getResources().getResourceEntryName(from.getId()).toString().equals(square3.toString())) {
                                        from.setBackgroundColor(getColor(R.color.move_color));
                                        continue;
                                    }
                                    if (getResources().getResourceEntryName(to.getId()).toString().equals(square3.toString())) {
                                        to.setBackgroundColor(getColor(R.color.move_color));
                                        continue;
                                    }
                                }
                                    if (square3.isLightSquare()) {
                                        getImageViewSquareLocation(square3).setBackgroundColor(getColor(R.color.white_square));
                                    } else {
                                        getImageViewSquareLocation(square3).setBackgroundColor(getColor(R.color.black_square));
                                    }
                                }
                                squaresLegals = new ArrayList<Square>();
                                square1 = square;
                                List<Move> moves = board.legalMoves();
                                for (Move move : moves) {
                                    if (move.getFrom().equals(square1)) {
                                        squaresLegals.add(move.getTo());
                                    }
                                }
                                for (Square square3 : squaresLegals) {
                                    if (square3.isLightSquare()) {
                                        getImageViewSquareLocation(square3).setBackgroundColor(Color.LTGRAY);
                                    } else {
                                        getImageViewSquareLocation(square3).setBackgroundColor(Color.GRAY);
                                    }
                                }
                                if (squaresLegals.isEmpty()) {
                                    square1 = null;
                                }
                            }
                            return;
                        } else {
                            if (from != null && to != null) {
                                for (Square squareAux : Square.values()) {
                                    if (squareAux.equals(Square.NONE)) continue;
                                    if (getResources().getResourceEntryName(from.getId()).toString().equals(squareAux.toString())) {
                                        if (squareAux.isLightSquare()) {
                                            from.setBackgroundColor(getColor(R.color.white_square));
                                        } else {
                                            from.setBackgroundColor(getColor(R.color.black_square));
                                        }
                                    }
                                    if (getResources().getResourceEntryName(to.getId()).toString().equals(squareAux.toString())) {
                                        if (squareAux.isLightSquare()) {
                                            to.setBackgroundColor(getColor(R.color.white_square));
                                        } else {
                                            to.setBackgroundColor(getColor(R.color.black_square));
                                        }
                                    }
                                }
                                from = null;
                                to = null;
                            }
                        }
                        Move move = null;
                        for (Square square3 : squaresLegals) {
                            if (square3.isLightSquare()) {
                                getImageViewSquareLocation(square3).setBackgroundColor(getColor(R.color.white_square));
                            } else {
                                getImageViewSquareLocation(square3).setBackgroundColor(getColor(R.color.black_square));
                            }
                            if (square2.equals(square3)) {
                                move = new Move(square1, square2);
                                if (getImageViewSquareLocation(move.getTo()).getDrawable() != null) {
                                    moveCapture.start();
                                    capture = true;
                                } else {
                                    capture = false;
                                }
                                if (board.getPiece(move.getFrom()).getPieceType() != null) {
                                    Piece piece = board.getPiece(move.getFrom());
                                    if (piece.getPieceType().equals(PieceType.PAWN)) {
                                        if (board.getSideToMove().equals(Side.BLACK) && move.getTo().getRank().equals(Rank.RANK_1)) {
                                            board.doMove(move);
                                            board.setPiece(Piece.BLACK_QUEEN, move.getFrom());
                                            getImageViewSquareLocation(square1).setImageResource(R.drawable.black_queen);
                                            board.doNullMove();
                                        } else if (board.getSideToMove().equals(Side.WHITE) && move.getTo().getRank().equals(Rank.RANK_8)) {
                                            board.doMove(move);
                                            board.setPiece(Piece.WHITE_QUEEN, move.getFrom());
                                            getImageViewSquareLocation(square1).setImageResource(R.drawable.white_queen);
                                            board.doNullMove();
                                        } else {
                                            if (board.getEnPassantTarget().equals(Square.NONE)) {
                                                getImageViewSquareLocation(square2).setImageDrawable(getImageViewSquareLocation(square1).getDrawable());
                                                getImageViewSquareLocation(square1).setImageDrawable(null);
                                                board.doMove(move);
                                            } else {
                                                Square square31 = board.getEnPassant();
                                                square2 = board.getEnPassantTarget();
                                                getImageViewSquareLocation(square31).setImageDrawable(getImageViewSquareLocation(square1).getDrawable());
                                                getImageViewSquareLocation(square1).setImageDrawable(null);
                                                getImageViewSquareLocation(square2).setImageDrawable(null);
                                                board.doMove(move);
                                            }
                                        }
                                    } else {
                                        if (board.getContext().isKingSideCastle(move)) {
                                            Move kingMove;
                                            Move rookMove;
                                            if (board.getSideToMove().equals(Side.WHITE)) {
                                                kingMove = board.getContext().getWhiteoo();
                                                rookMove = board.getContext().getWhiteRookoo();
                                            } else {
                                                kingMove = board.getContext().getBlackoo();
                                                rookMove = board.getContext().getBlackRookoo();
                                            }
                                            getImageViewSquareLocation(kingMove.getTo()).setImageDrawable(getImageViewSquareLocation(kingMove.getFrom()).getDrawable());
                                            getImageViewSquareLocation(kingMove.getFrom()).setImageDrawable(null);
                                            getImageViewSquareLocation(rookMove.getTo()).setImageDrawable(getImageViewSquareLocation(rookMove.getFrom()).getDrawable());
                                            getImageViewSquareLocation(rookMove.getFrom()).setImageDrawable(null);
                                            board.doMove(move);
                                        } else if (board.getContext().isQueenSideCastle(move)) {
                                            Move kingMove;
                                            Move rookMove;
                                            if (board.getSideToMove().equals(Side.WHITE)) {
                                                kingMove = board.getContext().getWhiteooo();
                                                rookMove = board.getContext().getWhiteRookooo();
                                            } else {
                                                kingMove = board.getContext().getBlackooo();
                                                rookMove = board.getContext().getBlackRookooo();
                                            }
                                            getImageViewSquareLocation(kingMove.getTo()).setImageDrawable(getImageViewSquareLocation(kingMove.getFrom()).getDrawable());
                                            getImageViewSquareLocation(kingMove.getFrom()).setImageDrawable(null);
                                            getImageViewSquareLocation(rookMove.getTo()).setImageDrawable(getImageViewSquareLocation(rookMove.getFrom()).getDrawable());
                                            getImageViewSquareLocation(rookMove.getFrom()).setImageDrawable(null);
                                            board.doMove(move);
                                        } else {
                                            getImageViewSquareLocation(square2).setImageDrawable(getImageViewSquareLocation(square1).getDrawable());
                                            getImageViewSquareLocation(square1).setImageDrawable(null);
                                            board.doMove(move);
                                        }
                                    }
                                }
                            }
                        }
                        if (!capture) {
                            moveSound.start();
                        }
                        squaresLegals = null;
                        square1 = null;
                        square2 = null;
                    }
                }
            });
        }
    }

     /** Este método é moi útil porque pásaslle un Board, o que ven sendo
      * unha partida, e el xa che pon perfectamente as pezas na vista do taboleiro (tableiro).
      *
      * @param board Obxecto usado para representar un taboleiro de xadrez (coas pezas, etc).
      */
    private void setPiecesOnBoard(Board board) {
        for (Square square : Square.values()) {
            if (square.equals(Square.NONE))
                continue;
            getImageViewSquareLocation(square).setImageDrawable(null);
            getImageViewSquareLocation(square).setAlpha(1f);
        }
        for (Piece piece : Piece.allPieces) {
            if (piece.equals(Piece.NONE)) continue;
            if (piece.equals(Piece.WHITE_KING)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.white_king);
                }
            } else if (piece.equals(Piece.WHITE_QUEEN)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.white_queen);
                }
            } else if (piece.equals(Piece.WHITE_PAWN)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.white_pawn);
                }
            } else if (piece.equals(Piece.WHITE_BISHOP)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.white_bishop);
                }
            } else if (piece.equals(Piece.WHITE_ROOK)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.white_rook);
                }
            } else if (piece.equals(Piece.WHITE_KNIGHT)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.white_knight);
                }
            } else if (piece.equals(Piece.BLACK_KING)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.black_king);
                }
            } else if (piece.equals(Piece.BLACK_QUEEN)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.black_queen);
                }
            } else if (piece.equals(Piece.BLACK_PAWN)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.black_pawn);
                }
            } else if (piece.equals(Piece.BLACK_BISHOP)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.black_bishop);
                }
            } else if (piece.equals(Piece.BLACK_ROOK)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.black_rook);
                }
            } else if (piece.equals(Piece.BLACK_KNIGHT)) {
                List<Square> squares = board.getPieceLocation(piece);
                for (Square square : squares) {
                    getImageViewSquareLocation(square).setImageResource(R.drawable.black_knight);
                }
            }
        }
    }

    /**
     * EXIT APPLICATION.
     * @param v Vista a que fai referencia, neste caso Botón saír.
     */
    public void onClickExit(View v) {
        finishAffinity();
    }

    /** Simplemente pásaslle un puzzle e crea o Board e añade ao Board os listener necesarios
     * como a resposta da máquina cando o player fai o movemento correcto. Tamén cambia as cores
     * dos botóns e fai a animación esa wapa se acerta ou resolve o puzzle.
     * @param puzzle Puzzle seleccionado para iniciar a partida con el. Ou o collido se é a primeira ves que se inisia.
     */
    private void startGame(Puzzle puzzle) {
        buttonUndo.setBackgroundColor(getColor(R.color.buttons_color));
        buttonChangePuzzle.setBackgroundColor(getColor(R.color.buttons_color));
        buttonChangePuzzle.setTextColor(Color.WHITE);
        board = new Board();
        board.loadFromFen(puzzle.getFen());
        actual = puzzle;
        solucionPuzzle = actual.getSolucion();
        finalMove = false;
        setPiecesOnBoard(board);
        addListeners();
        cont = 0;
        board.addEventListener(BoardEventType.ON_MOVE, new BoardEventListener() {
            @Override
            public void onEvent(BoardEvent event) {
                if (event.getType() == BoardEventType.ON_MOVE) {
                    if (board.isKingAttacked()) {
                        moveCheck.start();
                    }
                    Move move = (Move) event;
                    if (move.equals(solucionPuzzle.get(cont))) {
                        if (solucionPuzzle.size() == (cont + 1)) {
                            cont++;
                            textViewGame.setText(getString(R.string.toast_solved));
                            textViewTitle.setText(textViewTitle.getText().toString().substring(0,textViewTitle.getText().toString().length() - 2) + " \u2705");
                            buttonChangePuzzle.setBackgroundColor(Color.GREEN);
                            buttonChangePuzzle.setTextColor(Color.BLACK);
                            buttonChangePuzzle.startAnimation(scaleUp);
                            buttonChangePuzzle.startAnimation(scaleDown);
                            dataBase.setPuzzleSolved(actual);
                            finalMove = true;
                            for (Square square : Square.values()) {
                                if (square.equals(Square.NONE))
                                    continue;
                                getImageViewSquareLocation(square).setOnClickListener(null);
                            }

                        } else {
                            textViewGame.setText("");
                            cont++;
                            Move moveClone = solucionPuzzle.get(cont);
                            from = getImageViewSquareLocation(moveClone.getFrom());
                            to = getImageViewSquareLocation(moveClone.getTo());

                            board.removeEventListener(BoardEventType.ON_MOVE, this);
                            board.doMove(moveClone);
                            if (board.isKingAttacked()) {
                                moveCheck.start();
                            }
                            board.addEventListener(BoardEventType.ON_MOVE, this);
                            setPiecesOnBoard(board);
                            from.setBackgroundColor(getColor(R.color.move_color));
                            to.setBackgroundColor(getColor(R.color.move_color));
                            cont++;
                        }
                    } else {
                        cont++;
                        try {
                            for (Square square : Square.values()) {
                                if (square.equals(Square.NONE)) continue;
                                getImageViewSquareLocation(square).setOnClickListener(null);
                            }

                        } catch (NoSuchElementException e) {
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            finalMove = true;
                        }
                        textViewGame.setText(getString(R.string.toast_wrong_move));
                        buttonUndo.setBackgroundColor(Color.RED);
                        buttonUndo.startAnimation(scaleUp);
                        buttonUndo.startAnimation(scaleDown);
                    }
                }
            }
        });
        if (board.getSideToMove().toString().equals("WHITE")) {
            textViewPlayer.setText(getString(R.string.toast_move_white));
        } else {
            textViewPlayer.setText(getString(R.string.toast_move_black));
        }
    }

    /** Crea ou abre a DB
     *
     */
    private void crearDBOrOpenDB() {
        dataBase = new DataBase(getApplicationContext());
        dataBase.sqlLiteDB = dataBase.getWritableDatabase();
    }

    /** Inicializa todas as cousas e crea o Thread que descarga os XMLs e demáis.
     * Tamén comproba que non se lle pasara o intent con valores para saber se
     * ven doutra actividad ou é a primeira vez que arrancan a app.
     * Comproba que o usuario teña internet senón pois sale da aplicación.
     * @param savedInstanceState Bundle que non se usa. Suele usarse para gardar valores ao apagar a app e despois recollelos.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        AppCenter.start(getApplication(), "314433a4-15a2-4f8e-99f9-0e0e281eb5b7",
                Analytics.class, Crashes.class, Distribute.class);

        conexion = comprobarRede();
        if (conexion == TIPOREDE.SENREDE) {
            Toast.makeText(this, getString(R.string.toast_no_internet), Toast.LENGTH_LONG).show();
            if (dataBase == null || dataBase.getPuzzles().isEmpty()) {
                finish();
            }
        }

        moveSound = MediaPlayer.create(getApplicationContext(), R.raw.move);
        moveCapture = MediaPlayer.create(getApplicationContext(), R.raw.capture);
        moveCheck = MediaPlayer.create(getApplicationContext(), R.raw.check);
        moveWIN = MediaPlayer.create(getApplicationContext(), R.raw.puzzles_solved);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        textViewPlayer = (TextView) findViewById(R.id.text_view_player);
        textViewGame = (TextView) findViewById(R.id.text_view_puzzle);
        textViewTitle = (TextView) findViewById(R.id.text_view_puzzle_title);
        puzzles = new ArrayList<Puzzle>();

        animateButtons();
        addListeners();
        crearDBOrOpenDB();
        String dificultad = getIntent().getStringExtra("dificultadPuzzle");
        String title = getIntent().getStringExtra("title");
        if (dificultad != null && !dificultad.isEmpty()) {
            textViewTitle.setText(title);

            int puzzleBundleDificultad = Integer.parseInt(dificultad);

            for (Puzzle puzzle : dataBase.getPuzzles()) {
                if (puzzle.getDificultad() == puzzleBundleDificultad) {
                    startGame(puzzle);
                }
            }
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    descargarXML(XML);
                    boolean gameEnded = true;
                    for (Puzzle puzzle : dataBase.getPuzzles()) {
                        if (!puzzle.isResolto()) {
                            startGame(puzzle);
                            textViewTitle.setText(getGoodTitle());
                            gameEnded = false;
                            break;
                        }
                    }
                    if (gameEnded) {
                        startGame(dataBase.getPuzzles().get(dataBase.getPuzzles().size() - 1));
                        textViewTitle.setText(getGoodTitle());
                    }
                }
            });
        }
    }

    /** Os puzzles están ordenados aboleo, pero ti ao user quéreslle dar
     * os puzzles ordenados por dificultade. Esto devolve o título do puzzle co número ordenado.
     * @return Retorna o título(String) co número no orden da vista da selección de puzzles.
     */
    private String getGoodTitle() {
        String title = "";
        int cont = 0;

        for (Puzzle puzzle : dataBase.getPuzzles()) {
            cont++;
            if (puzzle.getDificultad() == actual.getDificultad()) {
                String check = puzzle.isResolto()?"   \u2705":"   \u274c";
                title = getString(R.string.puzzle_spinner) + " " + cont + " " +  getString(R.string.dificultade_spinner) + " " + actual.getDificultad() + check;
            }
        }

        return title;
    }

    /** Dalle vidilla aos botóns cando lle das click :)
     *
     */
    @SuppressLint("ClickableViewAccessibility")
    private void animateButtons() {
        buttonUndo = (Button) findViewById(R.id.undo_button);
        buttonChangePuzzle = (Button) findViewById(R.id.change_button);
        buttonExit = (Button) findViewById(R.id.home_button);
        scaleUp = AnimationUtils.loadAnimation(this, R.anim.scale_up);
        scaleDown = AnimationUtils.loadAnimation(this, R.anim.slace_down);

        buttonUndo.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    buttonUndo.startAnimation(scaleUp);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    buttonUndo.startAnimation(scaleDown);
                }

                return false;
            }
        });

        buttonExit.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    buttonExit.startAnimation(scaleUp);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    buttonExit.startAnimation(scaleDown);
                }

                return false;
            }
        });

        buttonChangePuzzle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    buttonChangePuzzle.startAnimation(scaleUp);
                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    buttonChangePuzzle.startAnimation(scaleDown);
                }

                return false;
            }
        });
    }

    /** Procesa os XML leendos e creando o archivo para gardalos
     *
     * @param file Archivo dónde se gardarán os XMLs
     */
    private void procesarXML(File file) {
        try {
            InputStream is = new FileInputStream(file);

            XmlPullParser parser = Xml.newPullParser();
            parser.setInput(is, "UTF-8");

            int evento = parser.nextTag();
            Puzzle puzzle = null;

            while (evento != XmlPullParser.END_DOCUMENT) {
                if (evento == XmlPullParser.START_TAG) {
                    if (parser.getName().equals("puzzle")) {
                        puzzle = new Puzzle();
                        evento = parser.nextTag();
                        puzzle.setIdPuzzle(Integer.parseInt(parser.nextText()));
                        evento = parser.nextTag();
                        puzzle.setFen(parser.nextText());
                        evento = parser.nextTag();
                        puzzle.setDificultad(Integer.parseInt(parser.nextText()));
                        evento = parser.nextTag();
                        puzzle.setCorXogador(parser.nextText());
                        evento = parser.nextTag();
                        puzzle.setResolto(Boolean.parseBoolean(parser.nextText()));
                        evento = parser.nextTag();
                        evento = parser.nextTag();
                        parser.nextText();
                        evento = parser.nextTag();
                        String moves = parser.nextText();
                        ArrayList<Move> movesA = new ArrayList<Move>();
                        if (moves.contains("/")) {
                            String[] split = moves.split("/");
                            for (String string : split) {
                                String square1S = string.substring(0, 2);
                                String square2S = string.substring(2);

                                movesA.add(stringsToMove(square1S, square2S));
                            }
                        } else {
                            String square1S = moves.substring(0, 2);
                            String square2S = moves.substring(2);

                            movesA.add(stringsToMove(square1S, square2S));
                        }
                        puzzle.setSolucion(movesA);
                    }
                }
                if (evento == XmlPullParser.END_TAG) {
                    if (parser.getName().equals("puzzle")) {
                        puzzles.add(puzzle);
                    }
                }
                evento = parser.next();
            }
            is.close();

            insertPuzzles(puzzles);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PROCESAR_DEPARTAMENTO", e.getMessage());
        }
    }

    /**
     * Pásaslle dous strings e converteos a Move. (Obviamente os Strings teñen que ter coordenadas do taboleiro de xadrez:
     * A1, A2, B5, C3, F7... SEMPRE EN MAIÚSCULAS
     * @param square1S Casilla da que se vai a mover cara outra en String.
     * @param square2S Casilla ata onde se vai a mover dende a outra en String.
     * @return Obxecto Move coas súas respectivas Squares dentro.
     */
    private Move stringsToMove(String square1S, String square2S) {
        Square square1 = null;
        Square square2 = null;

        for (Square square : Square.values()) {
            if (square.equals(Square.NONE)) continue;
            if (square.toString().equals(square1S)) {
                square1 = square;
                continue;
            }
            if (square.toString().equals(square2S)) {
                square2 = square;
                continue;
            }
        }

        return new Move(square1, square2);
    }

    /** Comproba que teña internet o teléfono.
     *
     * @return ENUM creado arriba que devolve o tipo de rede pero solo necesitamos saber se ten internet ou non.
     */
    private TIPOREDE comprobarRede() {
        NetworkInfo networkInfo = null;

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            switch (networkInfo.getType()) {
                case ConnectivityManager.TYPE_MOBILE:
                    return TIPOREDE.MOBIL;
                case ConnectivityManager.TYPE_ETHERNET:
                    return TIPOREDE.ETHERNET;
                case ConnectivityManager.TYPE_WIFI:
                    return TIPOREDE.WIFI;
            }
        }
        return TIPOREDE.SENREDE;
    }

    /**
     * Inserta os puzzles na base de datos.
     * @param puzzles Puzzles recollidos da lectura dos XMLs.
     */
    private void insertPuzzles(ArrayList<Puzzle> puzzles) {
        if (dataBase == null || puzzles.isEmpty()) {
            Toast.makeText(getApplicationContext(), "No se han encontrado puzzles en el XML", Toast.LENGTH_LONG).show();
            return;
        }
        for (Puzzle puzzle : puzzles) {
            try {
                dataBase.addPuzzle(puzzle);
            } catch (SQLiteConstraintException e) {
            }
        }
    }

    /**
     * Descarga os XMLs da páxina web (creada por min por certo).
     * @param xml URL da páxina dónde estarán os XMLs
     */
    private void descargarXML(String xml) {
        URL url = null;
        try {
            url = new URL(xml);
        } catch (MalformedURLException e1) {
            Toast.makeText(ChessPuzzles.this.getApplicationContext(), getString(R.string.toast_xml_failed_download_xml), Toast.LENGTH_LONG).show();
            return;
        }

        HttpURLConnection conn = null;
        String nomeArquivo = Uri.parse(xml).getLastPathSegment();
        File file = new File(getFilesDir().getAbsolutePath() + File.separator + "XMLS");
        file.mkdirs();
        rutaArquivo = new File(file + File.separator + nomeArquivo);
        try {

            conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            try {
                conn.setRequestMethod("GET");
            } catch (ProtocolException e) {
                Log.e("ERROR", e.getMessage());
            }
            conn.setDoInput(true);

            conn.connect();

            int response = conn.getResponseCode();

            if (response != HttpURLConnection.HTTP_OK) {
                Toast.makeText(ChessPuzzles.this.getApplicationContext(), getString(R.string.toast_xml_conexion_dept), Toast.LENGTH_LONG).show();
                return;
            }
            OutputStream os = new FileOutputStream(rutaArquivo);
            InputStream in = conn.getInputStream();
            byte data[] = new byte[1024];
            int count;
            while ((count = in.read(data)) != -1) {
                os.write(data, 0, count);
            }
            os.flush();
            os.close();
            in.close();
            conn.disconnect();
            Log.i("COMUNICACION", "ACABOU");
            procesarXML(rutaArquivo);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("COMUNICACION", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("COMUNICACION", e.getMessage());
        }
    }

    /** O listener do Botón UNDO que move para atrás o último movemento
     * e comproba que se poda facer se non non fai nada. Tamén cambia
     * colores de algún botón en caso de que se precise.
     * @param view Vista a que se refire, neste caso o botón UNDO.
     */
    public void onClickUndo(View view) {
        try {
            buttonUndo.setBackgroundColor(getColor(R.color.buttons_color));
            buttonChangePuzzle.setBackgroundColor(getColor(R.color.buttons_color));
            buttonChangePuzzle.setTextColor(Color.WHITE);
            if (from != null && to != null) {
                for (Square squareAux : Square.values()) {
                    if (squareAux.equals(Square.NONE)) continue;
                    if (getResources().getResourceEntryName(from.getId()).toString().equals(squareAux.toString())) {
                        if (squareAux.isLightSquare()) {
                            from.setBackgroundColor(getColor(R.color.white_square));
                        } else {
                            from.setBackgroundColor(getColor(R.color.black_square));
                        }
                    }
                    if (getResources().getResourceEntryName(to.getId()).toString().equals(squareAux.toString())) {
                        if (squareAux.isLightSquare()) {
                            to.setBackgroundColor(getColor(R.color.white_square));
                        } else {
                            to.setBackgroundColor(getColor(R.color.black_square));
                        }
                    }
                }
                from = null;
                to = null;
            }
            if (squaresLegals != null && !squaresLegals.isEmpty()) {
                for (Square square3 : squaresLegals) {
                    if (square3.isLightSquare()) {
                        getImageViewSquareLocation(square3).setBackgroundColor(getColor(R.color.white_square));
                    } else {
                        getImageViewSquareLocation(square3).setBackgroundColor(getColor(R.color.black_square));
                    }
                }
                squaresLegals = null;
                square1 = null;
                square2 = null;
            }
            textViewGame.setText("");
            board.undoMove();
            if (!finalMove) {
                board.undoMove();
                if (cont > 0) {
                    cont--;
                }
            } else {
                finalMove = false;
            }
        } catch (NoSuchElementException e) {
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            addListeners();
            setPiecesOnBoard(board);
        }
        if (cont > 0) {
            cont--;
        }
    }
}

