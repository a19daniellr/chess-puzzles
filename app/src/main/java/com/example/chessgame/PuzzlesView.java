package com.example.chessgame;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import java.util.ArrayList;

/** @author Daniel Lago Riomao
 * @version 2.0
 */
public class PuzzlesView extends AppCompatActivity {

    private ListView list;
    /**
     * Simplemente os títulos dos puzzles pero en String porque para o adapter da lista necesitas Strings non podes usar obxectos Puzzles.
     */
    private ArrayList<String> puzzlesString;

    /**
     *  Método o que se chama cando se crea a clase.
     *
     * @param savedInstanceState Bundle para recoller datos cando a app se apaga/reinicia (non se usa).
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.puzzles_view_layout);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        list = (ListView) findViewById(R.id.list_puzzles);
        refreshList();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Esto utilizase para crear a flecha que che leva a atrás na selección de puzzles.
     *
     * @param item O flecha do menú.
     * @return retorna true se se fai click no botón de retroceder.
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Refresca os valores da lista collendo os puzzles da base de datos.
     * Añade o adapter a Lista de Puzzles.
     * Chama ao método para añadir os listener á lista.
     */
    private void refreshList() {
        int cont = 0;
        DataBase dataBase = new DataBase(getApplicationContext());
        dataBase.sqlLiteDB = dataBase.getWritableDatabase();

        ArrayList<Puzzle> puzzles = dataBase.getPuzzles();
        puzzlesString = new ArrayList<String>();

        for (Puzzle puzzle : puzzles) {
            String check = puzzle.isResolto()?"   \u2705":"   \u274c";
            puzzlesString.add(getString(R.string.puzzle_spinner) + " " + ++cont + " " + getString(R.string.dificultade_spinner) + " " + puzzle.getDificultad() + check);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_selectable_list_item,
                puzzlesString){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                View view = super.getView(position, convertView, parent);
                TextView tv = (TextView) view.findViewById(android.R.id.text1);
                tv.setTextColor(Color.WHITE);
                return view;
            }
        };
        list.setAdapter(adapter);

        addListeners();
    }

    /**
     * Añade os listeners para cando lle das click na lista a un puzzle concreto.
     */
    private void addListeners() {
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                String [] split = list.getItemAtPosition(position).toString().split(" ");
                String dificultad = split[3];
                intent.putExtra("dificultadPuzzle", dificultad);
                intent.putExtra("title", list.getItemAtPosition(position).toString());
                intent.setClass(getApplicationContext(), ChessPuzzles.class);
                finishAffinity();
                startActivity(intent);
            }
        });
    }
}
