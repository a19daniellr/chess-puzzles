package com.example.chessgame;

import com.github.bhlangonijr.chesslib.move.Move;

import java.util.ArrayList;

/**
 * @author Daniel Lago Riomao
 * @version 1.2
 */
public class Puzzle {

    private int idPuzzle;
    private String fen;
    private int dificultad;
    private String corXogador;
    private boolean resolto;
    private ArrayList<Move> solucion;

    public Puzzle(int idPuzzle, String fen, int dificultad, String corXogador, boolean resolto, ArrayList<Move> solucion) {
        this.idPuzzle = idPuzzle;
        this.fen = fen;
        this.dificultad = dificultad;
        this.corXogador = corXogador;
        this.resolto = resolto;
        this.solucion = solucion;
    }

    @Override
    public String toString() {
        return "Puzzle " + idPuzzle + " difficult " + dificultad;
    }

    public Puzzle(int idPuzzle, String fen, int dificultad, String corXogador, ArrayList<Move> solucion) {
        this.idPuzzle = idPuzzle;
        this.fen = fen;
        this.dificultad = dificultad;
        this.corXogador = corXogador;
        this.solucion = solucion;
    }

    public Puzzle() {
    }

    public int getIdPuzzle() {
        return idPuzzle;
    }

    public void setIdPuzzle(int idPuzzle) {
        this.idPuzzle = idPuzzle;
    }

    public String getFen() {
        return fen;
    }

    public void setFen(String fen) {
        this.fen = fen;
    }

    public int getDificultad() {
        return dificultad;
    }

    public void setDificultad(int dificultad) {
        this.dificultad = dificultad;
    }

    public String getCorXogador() {
        return corXogador;
    }

    public void setCorXogador(String corXogador) {
        this.corXogador = corXogador;
    }

    public boolean isResolto() {
        return resolto;
    }

    public void setResolto(boolean resolto) {
        this.resolto = resolto;
    }

    public ArrayList<Move> getSolucion() {
        return solucion;
    }

    public void setSolucion(ArrayList<Move> solucion) {
        this.solucion = solucion;
    }
}
